# Flask

## 1. Erste Schritte

Installiere Flask in Thonny und führe folgendes Programm aus.

Dieses Programm ist eine einfache "Boilerplate", d.h. ein Grundgerüst, welches wir Stück für Stück erweitern und dann die Grundlage für dein eigenes Programm sein wird.

```python
from flask import Flask

app = Flask(__name__)
```

Es wird nun ein eigener Server gestartet und du kannst deine Anwendung im Browser öffnen.

## 2. Erstellen der ersten eigenen Route

Erstelle eine eigene Route, indem du das Programm wie folgt modifizierst:

```python
from flask import Flask

app = Flask(__name__)

@app.route("/") #oder dein dein eigener Pfad
def hello_world():  # beliebiger Funktionsname
    return "Hello world" # Der Text der unter der Route angezeigt wird.
```

## 3. Weitere Routen hinzufügen.

Füge auf die gleiche Art weitere Routen hinzu. Du kannst auch Platzhalter verwenden, die dann als Variablen verwendet werden.

```python
from flask import Flask

app = Flask(__name__)

@app.route("/") #oder dein dein eigener Pfad
def hello_world():  # beliebiger Funktionsname
    return "Hello world" # Der Text der unter der Route angezeigt wird.

@app.route("/hello/<string:username>")
def hello_user(username):
    return "Hello user " + username
```

**Wichtig!**: Die Funktion benötigt nun einen Parameter.

## 4. HTML

Du kannst auch HTML in deinen Ausgaben verwenden:

```python
from flask import Flask

app = Flask(__name__)

@app.route("/") #oder dein dein eigener Pfad
def hello_world():  # beliebiger Funktionsname
    return "Hello world" # Der Text der unter der Route angezeigt wird.

@app.route("/hello/<string:username>")
def hello_user(username):
    return "Hello <b>user</b> " + username
```

Eigentlich würde man aber ein komplettes HTML-Grundgerüst zurückgeben (mit HTML, HEAD und BODY-Tags) - Dies wird aber schnell unübersichtlich. Dafür werden wir später Templates verwenden.

## 5. Templates

Wir verwenden im folgenden Templates mit einer HTML-ähnlichen Sprache Namens Jinja. Dafür modifizieren wir zunächst die python-Datei

```python
from flask import Flask
from flask import render_template
app = Flask(__name__)

@app.route("/") #oder dein dein eigener Pfad
def hello_world():  # beliebiger Funktionsname
    return "Hello world" # Der Text der unter der Route angezeigt wird.

@app.route("/hello/<string:username>")
def hello_user(username):
    return render_template(
        "template.html",
        title = "Hello",
        user = username
    )
```

**Beachte**, dass du die Importe erweitern musst, um dort die Datei ```render_template```einzubinden.


Legt eine Datei template.html an mit folgendem Inhalt im **Unterordner(!)** ```templates```:

```python
<!doctype html>
<html>
    <head>
     <meta charset="utf-8">
     <title>{{title}}</title>
    </head>
    <body>
    Hello {{user}}
    </body>
</html>
```

Nun wird dieses HTML-Grundgerüst ausgeliefert.

## 6. Styles

Mit **CSS** kannst du deine Datei stylen:

Lege eine Datei ```style.css``` im Ordner static/css an. Füge dort styles hinzu.

Du kannst nun dein Grundgerüst erweitern:


```python
<!doctype html>
<html>
    <head>
     <meta charset="utf-8">
     <title>{{title}}</title>
     <link rel="stylesheet" type="text/css" href="/static/css/style.css">
    </head>
    <body>
    Hello {{user}}
    </body>
</html>
```

## 7. Datenbanken

Wir schreiben eine generelle Klasse, welche die Datenbankfunktionalität übernimmt.

Dies ist die Datei user.py, die im gleichen Verzeichnis wie unsere Hauptdatei liegt.

Die Klasse benötigt zunächst zwei Methoden

```python
class User:
    def __init__(self, username):
        self.username = username

    def to_db(self):
        pass

    @classmethod
    def from_db(cls):
        pass
```

### 7.1 Aus der Datenbank lesen.

Ergänze das Programm wie folgt:

```python
import sqlite3

class User:
    def __init__(self, username, firstname, lastname):
        self.username = username
        self.firstname = firstname
        self.lastname = lastname

    def to_db(self):
        pass

    @classmethod
    def from_db(cls, username):
        connection = sqlite3.connect("database.db") # Muss vorher angelegt werden.
        cursor = connection.cursor()
        sql = f"SELECT username FROM users WHERE username = {username}"
        cursor.execute(sql)
        row = cursor.fetchone()
        connection.close()
        return User(row[0], row[1], row[2])

user = User.from_db("asbl")
```

### 7.2 Aus der Datenbank lesen.

Ergänze das Programm wie folgt:


```python
import sqlite3

class User:
    def __init__(self, username, firstname, lastname):
        self.username = username
        self.firstname = firstname
        self.lastname = lastname

    def to_db(self):
        connection = sqlite3.connect("database.db") # Muss vorher angelegt werden.
        cursor = connection.cursor()
        sql = f"INSERT INTO users(username, firstname, lastname) VALUES ('{self.username}', '{self.firstname}', '{self.lastname}')"
        cursor.execute(sql)
        connection.commit()
        connection.close()

    @classmethod
    def from_db(cls, username):
        connection = sqlite3.connect("database.db") # Muss vorher angelegt werden.
        cursor = connection.cursor()
        sql = f"SELECT username FROM users WHERE username = {username}"
        cursor.execute(sql)
        row = cursor.fetchone()
        connection.close()
        return User(row[0], row[1], row[2])

user = User.from_db("asbl")
```

## 8. Ein Formular erstellen

Im letzten Schritt wird ein Formular erstellt, dass auf die Datenbankklasse zugreift:

```python
from flask import request
import user

# ....

@app.route("add_user", methods=["GET", "POST"])
def form():
    if request.method == "GET":
      # show form
    else:
      # send form
```

Dies wird nun ausformuliert:

```python
    if request.method == "GET":
        return '''
                  <form method="POST">
                      <div><label>Username: <input type="text" name="username"></label></div>
                      <div><label>Firstname: <input type="text" name="first_name"></label></div>
                      <div><label>Lastname: <input type="text" name="last_name"></label></div>
                      <input type="submit" value="Submit">
                  </form>'''
    else:
        username = request.form.get("username")
        firstname = request.form.get("first_name")
        lastname = request.form.get("last_name")
        user = User(username, firstname, lastname)
        user.to_db()
        return f"Benutzer {username} wurde hinzugefügt"
```

9. FAQ
-------

### Wie kann ich mein Formular per GET senden?

  Manchmal möchtest du dein Formular per GET-senden, z.B. bei einer Suche oder eine Auswahl. Deine Suchabfragen wären dann nicht nur mit
  `http://meineseite.de/suche` auffindbar, sondern z.B. mit `http://meineseite.de/suche?name=meier` - du könntest diese Links auch direkt verlinken.

  Du erreichst dies, indem du die `method` des Formulars auf `get` umschaltest:
  
  ```
 <form method="GET">
  <!--- hier kommt das Formular hin -->
 <input type="submit" value="Submit">
 </form>
 ```

 In Python kannst du dieses Formular nun abfragen mit der Methode:

```python
 suchergebnis= request.args.get("name")
```

Dies speichert in die Variable suchergebnis den GET-Paramter, der in der URL gefunden wurde (Für die URL `http://meineseite.de/suche?name=meier` würde in suchergebnis der Wert "meier" gespeichert. Für die URL `http://meineseite.de/suche` ist der Wert `None`.

Mit einer If-Abfrage kannst du überprüfen, ob ein Parameter übergeben wurde

```python
if suchergebnis: 
  pass # werte das Formular aus
else:
  pass # Zeige das Formular
```


  
