Videos 
======

* [Tutorial als Text](https://codeberg.org/a_siebel/flask_fuer_schueler/src/branch/master/tutorial.md)

* [Teil 1](https://www.youtube.com/watch?v=2Kp5lnkAngg)

* [Teil 2](https://www.youtube.com/watch?v=M8W1lRwo3fc)

* [Teil 3](https://www.youtube.com/watch?v=3g59Q2ggPFA)

* [Python: Datenbanken](https://www.youtube.com/watch?v=S9rxiXDZR_I&t=27s)

* [Teil 4](https://www.youtube.com/watch?v=HxsBT2Tpt7Q)